from discord.ext import commands
from discord.utils import get
from discord import Embed
import keys

def help_text(prefix):
    return f"{prefix}quiz *Play the shopkeeper's quiz to earn gold!*\n" \
           f"{prefix}gold *Check your gold*\n" \
           f"{prefix}shop *Browse the shop*\n" \
           f"{prefix}top  *See the top users*\n"

class DotaHerosBot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=lambda _, __: "d", case_insensitive=True)

        # Load cogs
        self.load_extension('cogs.admin')
        self.load_extension('cogs.error_handler')
        self.load_extension('cogs.emojis')
        self.load_extension('cogs.database')
        self.load_extension('cogs.shopkeeper_quiz')
        self.load_extension('cogs.heroes')
        self.load_extension('cogs.shop')
        self.remove_command('help')

        @commands.command(aliases=["?"])
        async def help(self, ctx):
            """ Sends help message """
            embed = Embed()
            embed.title = "Help"
            embed.description = help_text(ctx.prefix)
            await ctx.send(embed=embed)

        @self.event
        async def on_ready():
            await self.get_cog('Emojis').load_emojis() 
            print("Fully loaded and ready")

        @self.event
        async def on_guild_join(guild):
            channel = get(guild.text_channels, name="general")
            if len(guild.text_channels) > 0 and channel is None:
                channel = guild.text_channels[0]
            await self.help(guild)

def main():
    bot = DotaHerosBot()
    bot.run(keys.discord_token)

if __name__ == '__main__':
    main()
