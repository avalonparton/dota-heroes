import discord
from discord.ext import commands
from asyncio import TimeoutError


class Users(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.database = bot.get_cog('Database')

    @commands.command()
    async def info(self, ctx):
        gold = self.database.get_user_gold(ctx.author)
        hero = self.database_get_user_hero(ctx.author)
        embed = discord.Embed()
        embed.title = f"User info for {ctx.author.display_name}"
        embed.description = f"Gold: **{gold}**" + "\n"
        embed.description += f"Hero: **{hero.name}**"
        await ctx.send(embed=embed)

    @commands.command()
    async def start(self, ctx):
        embed = discord.Embed()
        embed.title = f"Welcome, {ctx.author.display_name}"
        header = "What would you like to do?"
        options = ["Choose a Hero"]
        text = ""
        for i, opt in options:
            text += f"*{i}. {opt}\n"
        embed.add_field(name=header, value=text)
        message = await ctx.send(embed=embed)
        
        def check(reaction, user):
            # Only match this user's reactions on this message
            if reaction.message == message and user == ctx.author:
                try:
                    if (int(msg.content)) in range(1, n+1):
                        return True
                    else:
                        return False
                except:
                    return False
            else:
                return False

        # Wait 45 seconds for the user to react
        try:
            correct_msg = await ctx.bot.wait_for('reaction', check=check, timeout=45)

        except TimeoutError:
            await message.edit(content=f"Request timed out (nothing happened).")\



def setup(bot):
    bot.add_cog(Users(bot))