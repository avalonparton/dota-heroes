from discord.ext import commands
from discord.utils import get


class Hero():
    def __init__(self, name):
        self.name = name

class Heroes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def test(self, ctx):
        await ctx.send("Axe attacks!\n" + self.ascii_hero(Hero("Axe")))

def setup(bot):
    print("Loading Heroes cog")
    bot.add_cog(Heroes(bot))
    print("Loaded Heroes cog")

# Run tests if script is called directly
def test():
    axe = Hero("Axe")
    print(cog.ascii_hero(axe))

if __name__ == "__main__":
    test()