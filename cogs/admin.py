""" Admin commands extension """
import discord
from discord.ext import commands
import os
import time

# DotA Heroes servers
SERVERS = [650182236490170369, 650182259248463907, 650180306782912533]

# Checks
async def author_is_plomdawg(ctx):
    """ Returns True if the author is plomdawg """
    return ctx.author.id == 163040232701296641

class Admin(commands.Cog):
    """ Admin cog

    Provides commands:
         ;reload ;setup_emojis
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["r"])
    @commands.check(author_is_plomdawg)
    async def reload(self, ctx):
        """ Reloads all cogs """
        async with ctx.typing():
            try:
                ctx.bot.reload_extension('cogs.admin')
                ctx.bot.reload_extension('cogs.database')
                ctx.bot.reload_extension('cogs.error_handler')
                ctx.bot.reload_extension('cogs.shopkeeper_quiz')
                ctx.bot.reload_extension('cogs.users')
                print("Reloaded cogs")
                await ctx.send("Reloaded cogs.")
            except Exception as e: # pylint: disable=bare-except
                await ctx.send(f"Failed to reload cogs: {e}")


def setup(bot):
    bot.add_cog(Admin(bot))