import discord
from discord.ext import commands

def get_items():
    """ All of the items in the shop are configured here """
    items = []
    items.append(Item("Midas", 2250, "Use the **midas** command to generate 160 gold"))
    items.append(Item("Tango", 90, "Restores 112 health (comes with 3)"))
    items.append(Item("Healing Salve", 110, "Restores 400 health"))
    return items


class Item():
    def __init__(self, name, gold, desc):
        self.name = name
        self.gold = gold
        self.desc = desc


class Shop(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.database = bot.get_cog('Database')
        self.emojis = bot.get_cog('Emojis')
        self.items = get_items()

    @commands.command()
    async def shop(self, ctx):
        gold = self.emojis.get("Gold")
        embed = discord.Embed()
        embed.title = f"Secret Shop"
        desc = ""
        for item in self.items:
            desc +=  f" - **{item.name}** *{item.gold}* {gold}\n"
        embed.description = desc
        embed.set_thumbnail(url="https://i.imgur.com/Xyf1VjQ.png") # shopkeeper's face
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Shop(bot))