# Cogs

* [Admin](admin.py) commands for the bot owner.
* [Database](database.py) database operations (dtop, dgold)
* [Error Handler](error_handler.py) handles errors for all commands
* [Shopkeeper Quiz](shopkeeper_quiz.py) contains the quiz (dquiz)

Each cog is built with the following skeleton

```python
from discord.ext import commands

# Defines the cog, which can be accessed with bot.get_cog('cogname')
class CogName(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    # Commands are added as class methods with the decorator
    @commands.command()
    async def echo(self, ctx, *args):
        """ Example command that repeats the input """
        if args:
            await ctx.send(' '.join(args))
        else:
            await ctx.send(f"Usage: {ctx.prefix}echo [text]")

# Called when the cog is loaded with bot.add_cog('cogname')
def setup(bot):
    bot.add_cog(CogName(bot))
```