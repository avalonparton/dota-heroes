#!/bin/bash
# deploy.sh - installs a service to run the bot

function echo_blue(){ echo -e "\n\033[1;34m$@\033[0m\n"; }

# Install apt packages
sudo add-apt-repository --yes ppa:deadsnakes/ppa
echo_blue "apt update -y"
sudo apt update -y
echo_blue "sudo apt install -y python3.8 python3-pip libffi-dev libnacl-dev"
sudo apt install -y python3.8 python3-pip libffi-dev libnacl-dev

# Install pip packages
sudo python3.8 -m pip install --upgrade -r ${CI_PROJECT_DIR}/requirements.txt

# Install snap packages
echo_blue "sudo snap install ffmpeg"
sudo snap install ffmpeg

# Install dotaheroes
echo_blue "----- INSTALLING dotaheroes -----"

WorkingDirectory=/opt/dotaheroes
InstallDirectory=/opt/dotaheroes
sudo mkdir -p ${WorkingDirectory} ${InstallDirectory}

# Clean install dir
echo_blue "Cleaning install directory:"
sudo rm -rfv ${InstallDirectory}/cogs
sudo rm -rfv ${InstallDirectory}/assets
sudo rm -rfv ${InstallDirectory}/dotaheroes.py
#sudo rm -rfv ${InstallDirectory}/database.sql*

# Move to install dir
echo_blue "Moving to install directory:"
sudo mv -v ${CI_PROJECT_DIR}/cogs ${InstallDirectory}
sudo mv -v ${CI_PROJECT_DIR}/assets ${InstallDirectory}
sudo mv -v ${CI_PROJECT_DIR}/dotaheroes.py ${InstallDirectory}

# Decode secret key file.
echo "${KEYFILE}" | base64 -d | sudo tee "${InstallDirectory}/keys.py" > /dev/null

### /etc/systemd/system/dotaheroes.service
echo_blue "Creating /etc/systemd/system/dotaheroes.service:"
cat << EOF | sudo tee /etc/systemd/system/dotaheroes.service
[Unit]
Description=dotaheroes
After=multi-user.target
[Service]
User=root
Group=root
Type=idle
WorkingDirectory=${WorkingDirectory}
ExecStart=/usr/bin/python3.8 -u ${InstallDirectory}/dotaheroes.py
Restart=always
RestartSec=1
[Install]
WantedBy=multi-user.target
EOF

#### Reload systemctl to trigger new service
sudo systemctl daemon-reload
sudo systemctl stop dotaheroes || true
sudo systemctl start dotaheroes.service
sudo systemctl enable dotaheroes.service
sleep 1

echo_blue "systemctl status"
systemctl status dotaheroes

echo_blue "----- dotaheroes INSTALLED SUCCESSFULLY -----"
