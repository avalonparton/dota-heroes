# DotA Heroes

Moved to github - https://github.com/plomdawg/dotabot



# Commands

* quiz - play shopkeeper's quiz to earn gold
* balance / bal / gold - check current gold
* top - see users with the most gold
* shop - list available items for purchase
* buy [item #] - buy an item in the shop
